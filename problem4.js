
function groupingbasedOnVitamins(items){
    try{
    var results = {};

    for (let item of items) {
        let vitamins = item.contains.split(',').map(vitamin => vitamin.trim());
        for (let vitamin of vitamins) {
            if (!results[vitamin]) {
                results[vitamin] = [];
            }
            results[vitamin].push(item.name);
        }
    }

    return results;
   }catch(error){
    console.error('Error is :', error.message);
   }
}

module.exports=groupingbasedOnVitamins;