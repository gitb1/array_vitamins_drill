function sortingOnTheBasisOfVitamins(items){
try {
    // Sort the items based on the number of vitamins they contain
   const results= items.sort((item1, item2) => {
        const vitamins1 = item1.contains.split(',').map(vitamin => vitamin.trim()).length;
        const vitamins2 = item2.contains.split(',').map(vitamin => vitamin.trim()).length;
        return vitamins2 - vitamins1; // Sort in descending order of vitamin count
    });
      
    return results;
   }catch(error){
    console.log('Error is :',error.message);
   }
}

module.exports= sortingOnTheBasisOfVitamins;