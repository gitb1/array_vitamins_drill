function itemsAvailable(items){
  try {
    const results=[];
  for( let item of items ){
    if(item.available===true){
        results.push(item.name)
    }
  }

  return results;
  }catch(error){
    console.log('Error is: ',error.message);
  }
}

module.exports=itemsAvailable;